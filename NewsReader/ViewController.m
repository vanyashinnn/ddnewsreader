//
//  ViewController.m
//  NewsReader
//
//  Created by Nikolay on 04/02/2017.
//  Copyright © 2017 Nikolay. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController
@synthesize listData;

-(void) didReceiveMyLocatlNotification {
    //Сообщение от AppDelegate
    NSLog(@"Update");
    //[_refreshControl endRefreshing];
}

- (void)viewDidLoad {
    [[NSNotificationCenter defaultCenter]addObserver:self
                                            selector:@selector(didReceiveMyLocatlNotification)
                                                name:@"appOnShow"
                                              object:nil];
    // Do any additional setup after loading the view, typically from a nib.
    NSLog(@"viewDidLoad");

    [super viewDidLoad];
    _newsList = [[NewsList alloc] init];
    _newsLabel.text = [_newsList mainNewsTitle];
    
    if([_newsList count]>0){
        Record * record = [_newsList recordAt:0];
        _newsHeader.text = [record title];
        [_browser loadHTMLString:[record content] baseURL:nil];
    }
    
    _refreshControl = [[UIRefreshControl alloc] init] ;
    [_refreshControl addTarget:self action:@selector(refresh)
             forControlEvents:UIControlEventValueChanged];
    [self.tableView setRefreshControl:_refreshControl];
}
-(void)refresh {
    [_newsList update];
    [_tableView reloadData];
    [_refreshControl endRefreshing];
}
-(void) viewDidUnload{
    _newsList = nil;
    [super viewDidUnload];
    
}
-(void) dealloc{
    [_newsList release];
    [_tableView release];
    [_newsLabel release];
    [_refreshControl release];
    [super dealloc];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    Record * record = [_newsList recordAt:(int)indexPath.row];
    _newsHeader.text = [record title];
    [_browser loadHTMLString:[record content] baseURL:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    NSLog(@"didReceiveMemoryWarning");
}

#pragma mark -
#pragma mark Table View data source methods
-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [_newsList count];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(UITableViewCell*) tableView:(UITableView *)tableView
        cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    int idx = (int)indexPath.row;
    static NSString * SimpleTableIdentifier = @"SimpleTableIdentifier";
    UITableViewCell * cell = [_tableView dequeueReusableCellWithIdentifier:SimpleTableIdentifier];
    if(cell == nil){
        cell = [[[UITableViewCell alloc]
                 initWithStyle:UITableViewCellStyleDefault
                 reuseIdentifier:SimpleTableIdentifier] autorelease];
    }
    Record * record = [_newsList recordAt:idx];
    cell.textLabel.text = [record title];
    return cell;
}
@end
