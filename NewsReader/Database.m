//
//  Database.m
//  NewsReader
//
//  Created by Nikolay on 24.02.17.
//  Copyright © 2017 Nikolay. All rights reserved.
//

#import "Database.h"

@implementation Database

- (id)init{
    NSLog(@"Database init");
    if(self = [super init]){
        NSArray *paths = NSSearchPathForDirectoriesInDomains
        (NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        //make a file name to write the data to using the documents directory:
        NSString *fileName = [NSString stringWithFormat:@"%@/news.db",
                              documentsDirectory];
        dbFileName = strdup([fileName cStringUsingEncoding:NSASCIIStringEncoding]);
        int error;
        error = sqlite3_open(dbFileName, &db);
        if(SQLITE_OK != error){
            NSLog(@"Can't open database: %s\n", sqlite3_errmsg(db));
            sqlite3_close(db);
        }
    }
    return self;
}
- (void)dealloc{
    free(dbFileName);
    sqlite3_close(db);
    [super dealloc];
}
-(int)execute:(NSString*)sql{
    return [self execute:sql callback:0 object:0];
}
-(int) execute: (NSString*) sql callback:(DbCallback) callback object:(void*) object{
    int error;

    char *zErrMsg = 0;
    error = sqlite3_exec(db, [sql UTF8String], callback, object, &zErrMsg);
    if(SQLITE_OK != error){
        fprintf(stderr, "SQL error: %s\n", zErrMsg);
        sqlite3_free(zErrMsg);
    }
    return error;
}
@end
