//
//  NewsList.h
//  NewsReader
//
//  Created by Nikolay on 18.02.17.
//  Copyright © 2017 Nikolay. All rights reserved.
//

#ifndef NewsList_h
#define NewsList_h
#import <UIKit/UIKit.h>
@class Database;

@interface Record: NSObject{
    NSString *title;
    NSString *content;
}
@property (strong, nonatomic) NSString *title;
@property (strong, nonatomic) NSString *content;
+ (id) NewRecord: (NSString*) title content:(NSString*) content ;
@end

@interface NewsList: NSObject
@property (strong, nonatomic) NSMutableArray *recordList;
@property (strong, nonatomic) NSString *mainNewsTitle;
@property (strong, nonatomic) Database *database;

- (int) count;
- (Record*) recordAt: (int) idx;
- (void)update;
@end


#endif /* NewsList_h */
