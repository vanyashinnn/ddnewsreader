//
//  main.m
//  NewsReader
//
//  Created by Nikolay on 04/02/2017.
//  Copyright © 2017 Nikolay. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    NSLog(@"main");
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
