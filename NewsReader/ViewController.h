//
//  ViewController.h
//  NewsReader
//
//  Created by Nikolay on 04/02/2017.
//  Copyright © 2017 Nikolay. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NewsList.h"

@interface ViewController : UIViewController
<UITableViewDelegate, UITableViewDataSource>
{
    //NSMutableArray * listData;
    NewsList * _newsList;
    UIRefreshControl * _refreshControl;
    NSArray * listData;
}

@property (strong, nonatomic) IBOutlet UILabel *newsHeader;
@property (strong, nonatomic) IBOutlet UIWebView *browser;
@property (nonatomic, retain) NSArray * listData;
@property (retain, nonatomic) IBOutlet UITableView *tableView;
@property (retain, nonatomic) IBOutlet UILabel *newsLabel;

@end

