//
//  AppDelegate.h
//  NewsReader
//
//  Created by Nikolay on 04/02/2017.
//  Copyright © 2017 Nikolay. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end
