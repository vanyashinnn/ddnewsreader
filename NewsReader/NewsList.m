//
//  NewsList.m
//  NewsReader
//
//  Created by Nikolay on 18.02.17.
//  Copyright © 2017 Nikolay. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import <libxml/xmlmemory.h>
#import <libxml/parser.h>
#import "Database.h"
#import "NewsList.h"

//-------------------Record----------------------------
@implementation Record
@synthesize title;
@synthesize content;
+(id)NewRecord:(NSString *)title content:(NSString *)content{
    Record * record = [[Record alloc] init];
    record.title = title;
    record.content = content;
    return record;
}

- (NSString *)description{
    return [NSString stringWithFormat:@"title: %@\n content: %@\n", self.title , self.content ];
}

- (void)dealloc{
    NSLog(@"record dealloc");
    [super dealloc];
}

@end
//-------------------Record----------------------------


static xmlNodePtr findItem(xmlNodePtr curr, const xmlChar *tagName){
    if(!curr){
        return NULL;
    }
    if(xmlStrcmp(curr->name, tagName) == 0){
        return curr;
    }
    if(NULL == curr->children || XML_CDATA_SECTION_NODE == curr->children->type || XML_TEXT_NODE == curr->type){
        if(NULL == curr->next){
            return findItem(curr->parent->next, tagName);
        }
        return findItem(curr->next, tagName);
    }
    if(XML_ELEMENT_NODE == curr->type){
        return findItem(curr->children, tagName);
    }
    return findItem(curr->next, tagName);
}

static int insertRecordToRecordList(void *obj, int argc, char **argv, char **azColName){
    NSMutableArray * recordList = (NSMutableArray *)obj;
    NSString * _title = [[[NSString alloc] init] autorelease];
    NSString * _content = [[[NSString alloc] init] autorelease];
    const char *value;
    int i;
    for(i=0; i<argc; i++){
        value = argv[i] ? argv[i] : "NULL";
        if(strcmp("title", azColName[i]) == 0){
            _title = [NSString stringWithUTF8String: value];
        }
        if(strcmp("content", azColName[i]) == 0){
            _content = [NSString stringWithUTF8String: value];
        }
    }
    if(nil != _title && nil != _content){
        Record * newRecord = [[Record NewRecord: _title content: _content] autorelease];
        [recordList insertObject: newRecord atIndex: [recordList count]];
    }
    return 0;
}


//-------------------NewsList--------------------------
@implementation NewsList
- (id)init{
    if(self = [super init]){
        _recordList = [[NSMutableArray alloc] init];
        _mainNewsTitle = [[NSString alloc] init];
        _database = [[Database alloc] init];
        _mainNewsTitle = @"Новости";
        [self createTeble];
        [self update];
    }
    return self;
}

-(Record *)recordAt:(int)idx{
    int n = (int)[_recordList count];
    if(idx < 0 || n < idx){
        NSLog(@"null recordAt");
        return nil;
    }
    return [_recordList objectAtIndex:idx ];
    
}
-(NSString*) rssUrl{
    return @"http://static.feed.rbc.ru/rbc/internal/rss.rbc.ru/rbc.ru/mainnews.rss";
}

- (BOOL)connectedToInternet
{
    NSURL *url = [NSURL URLWithString:[self rssUrl]];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"HEAD"];
    NSHTTPURLResponse *response;
    
    [NSURLConnection sendSynchronousRequest:request returningResponse:&response error: NULL];
    
    return ([response statusCode] == 200) ? YES : NO;
}

-(void) addItemToDb: (xmlNodePtr) node{
    xmlNodePtr titleNode = findItem(node, (const xmlChar*)"title");
    xmlNodePtr contentNode = findItem(node, (const xmlChar*)"description");
    xmlNodePtr linkNode = findItem(node, (const xmlChar*)"link");
    xmlNodePtr idNode = findItem(node, (const xmlChar*)"guid");
    xmlNodePtr dateNode = findItem(node, (const xmlChar*)"pubDate");
    if(
       NULL == titleNode ||
       NULL == contentNode ||
       NULL == titleNode->children ||
       NULL == contentNode->children||
       NULL == titleNode->children->content ||
       NULL == contentNode->children->content ||
       NULL == linkNode ||
       NULL == linkNode->children ||
       NULL == linkNode->children->content ||
       NULL == idNode ||
       NULL == idNode->children ||
       NULL == idNode->children->content ||
       NULL == dateNode ||
       NULL == dateNode->children ||
       NULL == dateNode->children->content
    ){
        return;
    }
    NSString * newsid = [NSString stringWithUTF8String: (const char*)idNode->children->content];
    NSString * pubdate = [NSString stringWithUTF8String: (const char*)dateNode->children->content];
    NSString * title = [NSString stringWithUTF8String: (const char*)titleNode->children->content];
    NSString * content = [NSString stringWithUTF8String: (const char*)contentNode->children->content];
    
    NSURL * url = [NSURL URLWithString:[NSString stringWithUTF8String: (const char*)linkNode->children->content]];
    content = [NSString stringWithFormat:@"%@<br/><a href=\"%@\">Перейти к новости (нужно подключение к интернету)</a>", content, url];

    [self insertNew: newsid pubdate:
        [self pubdateToStringWithFormat:pubdate format:@"yyyy-MM-dd HH:mm:ss"]
        title: title
        content: content
    ];
}
-(NSString*) pubdateToStringWithFormat: (NSString*) pubdate format:(NSString*) format{
    // convert datetime format
    NSString * result = [[[NSString alloc]init] autorelease];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"EEE, dd MMM yyyy HH:mm:ss ZZ"];
    NSLocale *enUSPOSIXLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [dateFormatter setLocale:enUSPOSIXLocale];
    NSDate *date = [dateFormatter dateFromString: pubdate];
    [dateFormatter setDateFormat: format];
    result = [dateFormatter stringFromDate: date];
    [dateFormatter release];
    return result;
}

#pragma mark -
#pragma mark Database requests
-(void)createTeble{
    NSString *sql = @"create table if not exists news (newsid nvarchar(255) not null primary key , pubdate date, title nvarchar(255), content text);";
    [_database execute:sql];
}
- (void)updateRecords{
    [_recordList removeAllObjects];
    NSString * sql = @"select * from news;";
    [_database execute:sql callback: insertRecordToRecordList object:(void*)_recordList ];
}
-(void) insertNew: (NSString*) newsid pubdate:(NSString*) pubdate title:(NSString*) title content:(NSString*) content{
    NSString *sql =
        [NSString stringWithFormat:@"insert into news (newsid, pubdate, title, content) values ('%@', '%@', '%@', '%@');",
        newsid ,pubdate, title, content];
    [_database execute:sql];
}
-(void) removeOld{
    //remove old then week
    NSString * sql = @"delete from news where julianday()-julianday(pubdate)>7;";
    [_database execute:sql];
}

#pragma mark -
-(void) getRss{
    if([self connectedToInternet] == NO){
        return;
    }
    NSLog(@"get RSS");
    NSAutoreleasePool * pool =
        [[NSAutoreleasePool alloc] init];
    NSError * error = nil;
    NSURL * url = [NSURL URLWithString:[self rssUrl]];
    NSString * urlContents =
        [NSString stringWithContentsOfURL:url
        encoding:NSUTF8StringEncoding error:&error];
    if(!urlContents){
        NSLog(@"empty contents");
        return;
    }
    const char * xmlStr = [urlContents cStringUsingEncoding:NSUTF8StringEncoding];
    xmlDocPtr doc = xmlParseMemory(xmlStr, strlen(xmlStr));
    xmlNodePtr curr;
    if(NULL == doc){
        return;
    }
    curr = xmlDocGetRootElement(doc);
    curr = findItem(curr, (const xmlChar*)"title");
    if(
       NULL != curr &&
       NULL != curr->children &&
       NULL != curr->children->content
    ){
        self.mainNewsTitle = [NSString stringWithUTF8String: (const char*)curr->children->content];
    }
    curr = findItem(curr, (const xmlChar*)"item");
    while(curr){
        [self addItemToDb: curr];
        curr=findItem(curr->next, (const xmlChar*)"item");
    }
    [pool release];
}
-(void)update{
    [self getRss];
    [self removeOld];
    [self updateRecords];
}
-(int)count{
    return [_recordList count];
}

@end
//-------------------NewsList--------------------------
