//
//  Database.h
//  NewsReader
//
//  Created by Nikolay on 24.02.17.
//  Copyright © 2017 Nikolay. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <sqlite3.h>
typedef int (*DbCallback)(void *obj, int argc, char **argv, char **azColName);

@interface Database : NSObject{
    sqlite3 * db;
    const char *dbFileName;
}
-(int) execute: (NSString*) sql;
-(int) execute: (NSString*) sql callback:(DbCallback) callback object:(void*) object;

@end
